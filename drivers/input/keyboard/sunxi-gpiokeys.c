#include <linux/module.h>
#include <linux/init.h>
#include <linux/input.h>
#include <linux/delay.h>
#include <linux/slab.h>
#include <linux/interrupt.h>
#include <linux/keyboard.h>
#include <linux/ioport.h>
#include <asm/irq.h>
#include <asm/io.h>
#include <linux/timer.h> 
#include <linux/clk.h>
#include <mach/sys_config.h>
#include <linux/gpio.h>
#include <linux/gpio_keys.h>
#include <linux/input.h>
#include <linux/platform_device.h>

#include <mach/sys_config.h>
#include <mach/platform.h>
#include <asm/io.h>


static struct gpio_keys_button* sunxi_key_buttons = NULL;
static char gpio_keys_desc[] = "sunxi btn";

static struct gpio_keys_platform_data sunxi_buttons_data = {
     .buttons = NULL,
     .nbuttons = 0,
     .rep = 1,
};

static struct platform_device sunxi_buttons_device = {
    .name = "gpio-keys",
    .id =-1,
    .dev = {
        .platform_data = &sunxi_buttons_data,
    }
};
    
static int __init sunxi_gpio_keys_init(void)
{
    int err = -1;
    int i = 0, cnt = 0;
	script_item_u val;
	script_item_value_type_e type;

    char gpio_buf[32] = "";
    char code_buf[32] = "";	
	int button_gpio = -1;
	int button_code = -1;

	type = script_get_item("gpio_keys", "key_used", &val);
	if (SCIRPT_ITEM_VALUE_TYPE_INT != type) {
		printk(KERN_ERR "%s: gpio_keys para not found, used default para. \n", __func__);
		return -1;
	}
	
	if(0 == val.val){
	    printk(KERN_ERR "%s: gpio_keys not enable. \n", __func__);
	    return -1;
	}
	
	type = script_get_item("gpio_keys", "key_cnt", &val);
	if (SCIRPT_ITEM_VALUE_TYPE_INT != type) {
		printk(KERN_ERR "%s: gpio_keys count para invalid, used default para. \n", __func__);
		return -1;
	}
	
	cnt = val.val;
	
    if(!cnt){
        printk(KERN_ERR "%s: these is zero number for gpio_keys' count. \n", __func__);
        return -1;
    }
    
    sunxi_key_buttons = kzalloc(sizeof(struct gpio_keys_button)*cnt, GFP_KERNEL);
    if(!sunxi_key_buttons){
        printk(KERN_ERR "%s: kzalloc fail for %d sunxi_key_buttons \n", __func__, cnt);
        return -1;
    }
    
    for(i=0; i<cnt; i++){
		sprintf(gpio_buf, "key%d_gpio", i);
		sprintf(code_buf, "key%d_code", i);
		
		// parse button gpio from gpio_keys
		type = script_get_item("gpio_keys", gpio_buf, &val);
		if (SCIRPT_ITEM_VALUE_TYPE_PIO != type)
			break;
		button_gpio = val.val;
        
        // parse button code from gpio_keys
		type = script_get_item("gpio_keys", code_buf, &val);
		if (SCIRPT_ITEM_VALUE_TYPE_INT != type)
			break;
		button_code = val.val;
		
		printk(KERN_INFO "%s: gpio_keys %d code %d, gpio %d \n", __func__, i, button_code, button_gpio);
        
        // button setup according to its gpio and code
        sunxi_key_buttons[i].gpio = button_gpio;
        sunxi_key_buttons[i].type = EV_KEY;
        sunxi_key_buttons[i].code = button_code;
        sunxi_key_buttons[i].desc = gpio_keys_desc;
        sunxi_key_buttons[i].active_low = 1;
    }

    printk(KERN_INFO "%s: gpio_keys count %d \n", __func__, i);
    sunxi_buttons_data.buttons = sunxi_key_buttons;
    sunxi_buttons_data.nbuttons = i+1;

    err = platform_device_register(&sunxi_buttons_device);
    
	if (IS_ERR_VALUE(err)) {
		printk(KERN_ERR "register sunxi gpio_keys platform driver failed\n");
		kfree(sunxi_key_buttons);
		return err;
	}
	
	return 0;
};

static void __exit sunxi_gpio_keys_exit(void)
{	
	platform_device_unregister(&sunxi_buttons_device);
	kfree(sunxi_key_buttons);
	printk(KERN_INFO "%s: gpio_keys module unloaded\n", __func__);
}

module_init(sunxi_gpio_keys_init);
module_exit(sunxi_gpio_keys_exit);

MODULE_AUTHOR("Phil Han <phil@focalcrest.com>");
MODULE_DESCRIPTION("sunxi gpio keys driver");
MODULE_LICENSE("GPL");


